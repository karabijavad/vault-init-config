This image assumes you have the kubernetes auth backend enabled in vault.

Required env vars:
* VAULT_ADDR
* VAULT_ROLE
* VAULT_SECRET (the name of the secret your config is stored in)

1. share a memdisk volume between the vault-init-config initContainer and app container.
2. in the initContainer, mount the volume at /vault-init-config
3. in the app container, mount the volume at the location you want the config file to be placed (including the filename)

the initcontainer will login to vault using the kubernetes service account token, then request the secret and place its data as json into /vault-init-container/config.json

```yaml
spec:
  template:
    ...
    spec:
      ...
      volumes:
      - name: my-secrets-volume
        emptyDir: {
          medium: Memory
        }
      initContainers:
        - name: vault-init-config
          image: karabijavad/vault-init-config:latest
          env:
            - name: VAULT_ADDR
              value: https://vault:8200
            - name: VAULT_SKIP_VERIFY
              value: "1"
            - name: VAULT_ROLE
              value: my_vault_role
            - name: VAULT_SECRET
              value: my_vault_secret
          volumeMounts:
            - name: my-secrets-volume
              mountPath: /vault-init-config
      containers:
        - name: my-app
          ...
          volumeMounts:
            - name: my-secrets-volume
              mountPath: /my_app/config.json
              subPath: config.json
          ....
```