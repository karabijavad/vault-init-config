#!/bin/sh

export VAULT_TOKEN=$(
    vault write -field=token auth/kubernetes/login \
        role=$VAULT_ROLE \
        jwt=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
);

vault read -format=json secret/$VAULT_SECRET \
    | jq '.data' \
    > /vault-init-config/config.json
