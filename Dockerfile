FROM busybox

RUN mkdir -p /usr/local/bin/

RUN wget https://releases.hashicorp.com/vault/0.8.3/vault_0.8.3_linux_amd64.zip &&\
    unzip vault_0.8.3_linux_amd64.zip -d /usr/local/bin/ && \
    chmod +x /usr/local/bin/vault && \
    rm vault_0.8.3_linux_amd64.zip

RUN wget https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64 &&\
    mv jq-linux64 /usr/local/bin/jq && \
    chmod +x /usr/local/bin/jq

ADD ./docker-entrypoint.sh /docker-entrypoint.sh

RUN chmod +x /docker-entrypoint.sh

RUN mkdir -p /vault-init-config
VOLUME ["/vault-init-config"]

ENTRYPOINT [ "/docker-entrypoint.sh" ]
